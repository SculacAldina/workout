import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, Text} from 'react-native';
import {NativeStackHeaderProps} from '@react-navigation/native-stack';
import ExerciseForm, {ExerciseFormData} from '../components/ExerciseForm';
import {SequenceItem, SequenceType, Workout} from '../types/data';
import slugify from 'slugify';
import ExerciseItem from '../components/ExerciseItem';
import {PressableText} from '../components/styled/PressableText';
import {ModalComponent} from '../components/styled/ModalComponent';
import WorkoutForm, {WorkoutFormData} from '../components/WorkoutForm';
import {storeWorkout} from '../storage/workout';
import {PressableThemeText} from '../components/PressableThemeText';

const PlannerScreen = ({navigation}: NativeStackHeaderProps) => {
  // useEffect(() => {
  //   console.log('Rendering Planner Screen');
  //   return () => console.log('Unmounting Planner');
  // }, []);

  const [seqItems, setSeqItems] = useState<SequenceItem[]>([]);

  const handleExerciseSubmit = (form: ExerciseFormData) => {
    const sequenceItem: SequenceItem = {
      slug: slugify(form.name + ' ' + Date.now(), {lower: true}),
      name: form.name,
      type: form.type as SequenceType,
      duration: Number(form.duration),
    };

    if (form.reps) {
      sequenceItem.reps = Number(form.reps);
    }

    setSeqItems([...seqItems, sequenceItem]);
  };

  const computeDiff = (exercisesCount: number, workoutDuration: number) => {
    const intensity = workoutDuration / exercisesCount;

    if (intensity <= 60) {
      return 'hard';
    } else if (intensity <= 100) {
      return 'normal';
    } else {
      return 'easy';
    }
  };

  const handleWorkoutSubmit = async (form: WorkoutFormData) => {
    if (seqItems.length > 0) {
      const duration = seqItems.reduce((acc, item) => {
        return acc + item.duration;
      }, 0);

      const workout: Workout = {
        name: form.name,
        slug: slugify(form.name + ' ' + Date.now(), {lower: true}),
        difficulty: computeDiff(seqItems.length, duration),
        sequence: [...seqItems],
        duration: duration,
      };

      // console.log(workout);
      await storeWorkout(workout);
    }
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={seqItems}
        keyExtractor={item => item.slug}
        renderItem={({item, index}) => (
          <ExerciseItem item={item}>
            <PressableText
              title="Remove"
              onPressIn={() => {
                const items = [...seqItems];
                items.splice(index, 1);
                setSeqItems(items);
              }}
            />
          </ExerciseItem>
        )}
      />
      <ExerciseForm onSubmit={handleExerciseSubmit} />
      <View>
        <ModalComponent
          activator={({handleOpen}) => (
            <PressableThemeText
              style={{marginTop: 15}}
              title="Create Workout"
              onPress={handleOpen}
            />
          )}>
          {({handleClose}) => (
            <View>
              <WorkoutForm
                onSubmit={async data => {
                  await handleWorkoutSubmit(data);
                  handleClose();
                  navigation.navigate('Home');
                }}
              />
            </View>
          )}
        </ModalComponent>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});

export default PlannerScreen;

{
  /* <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />; */
}
