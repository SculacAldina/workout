import React, {useEffect, useState} from 'react';
import {Pressable, View, StyleSheet, FlatList} from 'react-native';
import {NativeStackHeaderProps} from '@react-navigation/native-stack';
import WorkoutItem from '../components/WorkoutItem';
import {SpecialText} from '../components/styled/SpecialText';
import {useWorkouts} from '../hooks/useWorkouts';
import {ThemeText} from '../components/Text';

const HomeScreen = ({navigation}: NativeStackHeaderProps) => {
  // useEffect(() => {
  //   console.log('Rendering Home Screen');
  //   return () => console.log('Unmounting Home');
  // }, []);

  const workouts = useWorkouts();
  return (
    <View style={styles.container}>
      <ThemeText style={{color: '#333'}}>Start Workout</ThemeText>
      <FlatList
        data={workouts}
        keyExtractor={item => item.slug}
        renderItem={({item}) => {
          return (
            <Pressable
              onPress={() =>
                navigation.navigate('WorkoutDetail', {slug: item.slug})
              }>
              <WorkoutItem item={item} />
            </Pressable>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});

export default HomeScreen;
//<Text>{JSON.stringify(data)}</Text>
