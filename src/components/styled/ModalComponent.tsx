import React, {Children, FunctionComponent, useState} from 'react';
import {Text, View, StyleSheet, Modal} from 'react-native';
import {PressableText} from './PressableText';

type ModalProps = {
  activator?: FunctionComponent<{
    handleOpen: () => void;
  }>; // functional component // ? optional param
  children: FunctionComponent<{
    handleClose: () => void;
    handleOpen: () => void;
  }>;
};

export const ModalComponent = ({
  activator: Activator,
  children,
}: ModalProps) => {
  const [showModal, setShowModal] = useState(false);

  const handleOpen = () => setShowModal(true);
  const handleClose = () => setShowModal(false);

  return (
    <>
      <Modal visible={showModal} transparent={false} animationType="slide">
        <View style={styles.centerView}>
          <View style={styles.contentView}>
            {children({handleClose, handleOpen})}
          </View>
          <PressableText title="Close" onPress={handleClose} />
        </View>
      </Modal>
      {Activator ? (
        <Activator handleOpen={handleOpen} />
      ) : (
        <PressableText title="Open" onPress={handleClose} />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  centerView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentView: {
    marginBottom: 20,
  },
});
