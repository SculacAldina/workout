import React from 'react';
import {Text, StyleSheet} from 'react-native';

// React.ReactNode === Text['props']['children'];

export function SpecialText(props: Text['props']) {
  return <Text {...props} style={[props.style, styles.text]} />;
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    marginBottom: 20,
    fontWeight: 'bold',
  },
});
