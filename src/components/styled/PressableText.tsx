import React from 'react';
import {
  Text,
  StyleSheet,
  Pressable,
  PressableProps,
  StyleProp,
  TextStyle,
} from 'react-native';

export type PressableTextProps = PressableProps & {
  title: string;
  style?: StyleProp<TextStyle>;
};

export function PressableText(props: PressableTextProps) {
  return (
    <Pressable {...props}>
      <Text style={[props.style, styles.btn]}>{props.title}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  btn: {
    textDecorationLine: 'underline',
  },
});
