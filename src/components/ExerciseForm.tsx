import React, {useEffect, useState} from 'react';
import {Text, View, StyleSheet, TextInput} from 'react-native';
import {useForm, Controller} from 'react-hook-form';
import {PressableText} from './styled/PressableText';

export type ExerciseFormData = {
  name: string;
  duration: string;
  reps?: string;
  slug: string;
  type: string;
};

type WorkoutProps = {
  onSubmit: (form: ExerciseFormData) => void;
};

const selectionItems = ['Exercise', 'Break', 'Stretch'];

export default function ExerciseForm({onSubmit}: WorkoutProps) {
  const {control, handleSubmit} = useForm();
  const [isSelectionOn, setIsSelectionOn] = useState(false);

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.rowContainer}>
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            name="name"
            render={({field: {onChange, value}}) => (
              <TextInput
                onChangeText={onChange}
                style={styles.input}
                value={value}
                placeholder="Name"
                placeholderTextColor={'rgba(0,0,0,0.4)'}
              />
            )}
          />
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            name="duration"
            render={({field: {onChange, value}}) => (
              <TextInput
                onChangeText={onChange}
                style={styles.input}
                value={value}
                placeholder="Duration"
                placeholderTextColor={'rgba(0,0,0,0.4)'}
              />
            )}
          />
        </View>
        <View style={styles.rowContainer}>
          <Controller
            control={control}
            name="reps"
            render={({field: {onChange, value}}) => (
              <TextInput
                onChangeText={onChange}
                style={styles.input}
                value={value}
                placeholder="Repetitions"
                placeholderTextColor={'rgba(0,0,0,0.4)'}
              />
            )}
          />
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            name="type"
            render={({field: {onChange, value}}) => (
              <View style={{flex: 1}}>
                {isSelectionOn ? (
                  <View>
                    {selectionItems.map(selection => (
                      <PressableText
                        key={selection}
                        title={selection}
                        style={styles.selection}
                        onPressIn={() => {
                          onChange(selection);
                          setIsSelectionOn(false);
                        }}
                      />
                    ))}
                  </View>
                ) : (
                  <TextInput
                    onPressIn={() => setIsSelectionOn(true)}
                    style={styles.input}
                    placeholder="Type"
                    placeholderTextColor={'rgba(0,0,0,0.4)'}
                    value={value}
                  />
                )}
              </View>
            )}
          />
        </View>
        <PressableText
          style={{marginTop: 10}}
          title="Add Exercise"
          onPress={handleSubmit(data => {
            onSubmit(data as ExerciseFormData);
          })}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
  },
  input: {
    flex: 1,
    margin: 2,
    borderWidth: 1,
    height: 30,
    padding: 5,
    borderRadius: 5,
    borderColor: 'rgba(0,0,0,0.4)',
  },
  rowContainer: {
    flexDirection: 'row',
  },
  selection: {
    margin: 2,
    padding: 3,
    alignSelf: 'center',
  },
});

// import React, {useEffect, useState} from 'react';
// import {Text, View, StyleSheet, TextInput} from 'react-native';
// import {PressableText} from './styled/PressableText';

// export type ExerciseForm = {
//   name: string;
//   duration: string;
// };

// type WorkoutProps = {
//   onSubmit: (form: ExerciseForm) => void;
// };

// export default function WorkoutForm({onSubmit}: WorkoutProps) {
//   return (
//     <View style={styles.container}>
//       <Text>Exercise Form</Text>
//       <View>
//         <TextInput
//           style={styles.input}
//           value={form.name}
//           placeholder="Name"
//           onChangeText={onChangeText('name')}
//         />
//         <TextInput
//           style={styles.input}
//           value={form.duration}
//           placeholder="Duration"
//           onChangeText={onChangeText('duration')}
//         />
//       </View>
//       <PressableText title="Submit" onPress={() => onSubmit(form)} />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: '#fff',
//     borderRadius: 10,
//     padding: 10,
//   },
//   input: {
//     height: 40,
//     margin: 12,
//     borderWidth: 1,
//     padding: 10,
//   },
// });
