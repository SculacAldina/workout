import React, {useEffect, useState} from 'react';
import {Workout} from '../types/data';
import {getWorkoutsBySlug} from '../storage/workout';

export const useWorkoutBySlug = (slug: string) => {
  const [workout, setWorkout] = useState<Workout>();

  useEffect(() => {
    async function getData() {
      const _workout = await getWorkoutsBySlug(slug);
      setWorkout(_workout);
    }
    getData();
  }, []);

  return workout;
};
