import React, {useEffect, useState} from 'react';
import {initWorkouts, getWorkouts, clearWorkouts} from '../storage/workout';

export default function useCachedResources() {
  const [isLoadingComplete, setIsLoadingComplete] = useState(false);

  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        // await clearWorkouts();
        await initWorkouts();
      } catch (e) {
        console.warn(e);
      } finally {
        const workouts = await getWorkouts();
        // console.log(workouts);
        setIsLoadingComplete(true);
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  // console.log('Returning: ', isLoadingComplete);
  return isLoadingComplete;
}

// V. I
// useEffect(() => {
//   // console.log('useEffect executed');
//   function loadResourcesAndDataAsync() {
//     setTimeout(() => {
//       // console.log('Setting isLoaded to True');
//       setIsLoadingComplete(true);
//     }, 500);

//     // try {
//     //   // Load Fonts
//     // } catch (e) {
//     //   console.warn(e);
//     // } finally {
//     //   setIsLoadingComplete(true);
//     // }
//   }

//   loadResourcesAndDataAsync();
// }, []);

// V. II
// useEffect(() => {
//   async function loadResourcesAndDataAsync() {
//     try {
//       const hasWorkouts = await containsKey('workout-data');
//       // If there is no workouts store workouts
//       if (!hasWorkouts) {
//         console.log('Storing data');
//         await storeData('workout-data', data);
//       }
//     } catch (e) {
//       console.warn(e);
//     } finally {
//       const workouts = await getData('workout-data');
//       console.log(workouts);
//       setIsLoadingComplete(true);
//     }
//   }

//   loadResourcesAndDataAsync();
// }, []);
