import React from 'react';
import useCachedResources from './src/hooks/useCachedResources';
import Navigation from './src/navigation/index';
import {useColorScheme} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const App = () => {
  const isLoaded = useCachedResources();
  const colorScheme = useColorScheme();
  console.log(colorScheme);

  if (isLoaded) {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
      </SafeAreaProvider>
    );
  } else {
    return null;
  }
};

export default App;
